﻿
## Image Gallery

**Requirements**
* An image gallery implementation with 3 image categories and different grid layouts based on the category images number. 

**IDE used**
* Visual Studio 2019 for Mac

**Deliverables**
* Android app
* iOS app
* No UWP app due to the use of VS for Mac

**Libraries and Frameworks**
* Xamarin Forms
* Prism with DryIoc IoC container
* FFImageLoading

**UI/UX**
* Clean and sleek UI
* Animations for image presentation
* Simple & descriptive interface 
* Supports orientation changes

**Architecture**
* Used MVVM and DI 
* Followed SOLID and DRY principles
* All views are XAML based
* Data driven implementation

**Approach**
* The app is driven by the images in the Resources folder of the shared project
* The ``IDataProvider`` implementation scans the shared assembly for jpg images and groups them into categories by name
* The ``TabsControl`` generates the buttons based on the number of categories resolved
* The first category is auto-selected by default
* The ``ImageGrid`` gets its ``ItemsSource`` property updated based on the selected category. It supports a maximum of 9 images as per the requirements
* An implementation of the ``IGridLayouterFactory`` is injected in the ``ImageGrid`` which will resolve the corresponding ``IGridLayouter`` based on the number of images of the selected category
* The implementation of the ``IGridLayouter`` will set the ``Row``, ``Column``, ``RowSpan`` and ``ColumnSpan`` of each child added as per the design requirements
* A fullscreen image view will be presented after each image tap and will be closed with a second tap

**Unit Tests (TODO)**
* ImageGrid
* TabsControl
* DataProvider
* GridLayouters


