﻿using ImageGallery.Extensions;
using ImageGallery.GridLayout.Abstractions;
using ImageGallery.Services;
using ImageGallery.Services.Abstractions;
using Prism;
using Prism.Ioc;
using Xamarin.Forms;

[assembly: ExportFont("materialdesignicons-webfont.ttf", Alias = "Material")]
namespace ImageGallery
{
    [AutoRegisterForNavigation]
    public partial class App
    {
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer)
        {
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            await NavigationService.NavigateToHome();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IGridLayouterFactory, GridLayouterFactory>();
            containerRegistry.Register<IDataProvider, DataProvider>();
        }
    }
}
