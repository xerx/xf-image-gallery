﻿using System;
using System.Windows.Input;
using ImageGallery.Models;

namespace ImageGallery.ViewModels
{
    public class ImageViewModel : IDisposable
    {
        public ICommand SelectionCommand { get; }
        public string Path => image.Path;
        private Image image;

        public ImageViewModel(Image image, ICommand selectionCommand)
        {
            this.image = image;
            SelectionCommand = selectionCommand;
        }

        public void Dispose()
        {
            image = null;
        }
    }
}
