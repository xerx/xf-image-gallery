﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using ImageGallery.GridLayout.Abstractions;
using ImageGallery.Services.Abstractions;
using Prism.Navigation;
using Xamarin.Forms;

namespace ImageGallery.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {
        private List<ImageViewModel> _images;
        public List<ImageViewModel> Images
        {
            get => _images;
            set => SetProperty(ref _images, value);
        }
        private ImageGroupViewModel _selectedGroup;
        public ImageGroupViewModel SelectedGroup
        {
            get => _selectedGroup;
            set
            {
                if (SetProperty(ref _selectedGroup, value))
                {
                    Images = _selectedGroup.Images.ToList();
                }
            }
        }
        private ImageSource _selectedImage;
        public ImageSource SelectedImage
        {
            get => _selectedImage;
            set => SetProperty(ref _selectedImage, value);
        }
        public List<ImageGroupViewModel> ImageGroups { get; set; }
        public IGridLayouterFactory LayouterFactory { get; set; }
        public ICommand SelectionCommand { get; set; }

        public HomePageViewModel(INavigationService navigationService,
                                 IGridLayouterFactory layouterFactory,
                                 IDataProvider dataProvider) : base(navigationService)
        {
            LayouterFactory = layouterFactory;
            SelectionCommand = new Command<ImageViewModel>(ImageSelected);
            ImageGroups = dataProvider.GetImageGroups()
                                      .Select(group => new ImageGroupViewModel(group, SelectionCommand))
                                      .ToList();
        }

        private void ImageSelected(ImageViewModel viewModel)
        {
            SelectedImage = ImageSource.FromResource(viewModel.Path);
        }

        public override void Destroy()
        {
            ImageGroups.ForEach(group => group.Dispose());
            base.Destroy();
        }
    }
}
