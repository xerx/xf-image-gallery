﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using ImageGallery.Models;
using Prism.Mvvm;

namespace ImageGallery.ViewModels
{
    public class ImageGroupViewModel : BindableBase, IDisposable
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }
        public string Icon => imageGroup.Icon;
        public List<ImageViewModel> Images { get; set; }

        private ImageGroup imageGroup;

        public ImageGroupViewModel(ImageGroup imageGroup, ICommand selectionCommand)
        {
            this.imageGroup = imageGroup;
            Images = imageGroup.Images
                               .Select(image => new ImageViewModel(image, selectionCommand))
                               .ToList();
        }

        public void Dispose()
        {
            Images.ForEach(vm => vm.Dispose());
            imageGroup = null;
        }
    }
}
