﻿using System;
using System.Threading.Tasks;
using ImageGallery.Extensions;
using Xamarin.Forms;

namespace ImageGallery.Views
{
    public partial class ImageOverlay : ContentView
    {
        #region Source
        public static BindableProperty SourceProperty =
                    BindableProperty.Create(nameof(Source),
                                            typeof(ImageSource),
                                            typeof(ImageOverlay),
                                            defaultBindingMode: BindingMode.TwoWay,
                                            propertyChanged: (b, o, n) => (b as ImageOverlay).SourceChanged());
        private void SourceChanged()
        {
            if(Source != null)
            {
                Show().FireAndForget();
            }
            else
            {
                Hide().FireAndForget();
            }
        }
        public ImageSource Source
        {
            get => (ImageSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }
        #endregion
        public ImageOverlay()
        {
            InitializeComponent();
            Hide(0).FireAndForget();
        }
        private Task Show(uint duration = 250)
        {
            image.Source = Source;
            IsVisible = true;
            return this.ScaleTo(1, duration, Easing.SinOut);
        }
        private async Task Hide(uint duration = 250)
        {
            await this.ScaleTo(0, duration, Easing.SinIn);
            IsVisible = false;
            image.Source = null;
        }

        private void Tapped(object sender, EventArgs e)
        {
            Source = null;
        }
    }
}
