﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using ImageGallery.ViewModels;
using Xamarin.Forms;

namespace ImageGallery.Views
{
    public partial class TabsControl : StackLayout
    {
        #region Selection
        public static BindableProperty SelectionProperty =
                    BindableProperty.Create(nameof(Selection),
                                            typeof(ImageGroupViewModel),
                                            typeof(TabsControl),
                                            defaultBindingMode: BindingMode.OneWayToSource);
        public ImageGroupViewModel Selection
        {
            get => (ImageGroupViewModel)GetValue(SelectionProperty);
            set => SetValue(SelectionProperty, value);
        }
        #endregion

        private IEnumerable<ImageGroupViewModel> itemsSource;

        public TabsControl()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if(propertyName == BindableLayout.ItemsSourceProperty.PropertyName)
            {
                itemsSource = BindableLayout.GetItemsSource(this).Cast<ImageGroupViewModel>();
                UpdateSelection(itemsSource.First());
            }
        }

        private void UpdateSelection(ImageGroupViewModel viewModel)
        {
            foreach (var item in itemsSource)
            {
                item.IsSelected = item == viewModel;
            }
            Selection = viewModel;
        }

        private void TabButtonSelected(object sender, TabEventArgs e)
        {
            UpdateSelection(e.ViewModel);
        }
    }
}
