﻿using System;
using ImageGallery.ViewModels;
using Xamarin.Forms;

namespace ImageGallery.Views
{
    public partial class TabButton : ContentView
    {
        #region IsSelected
        public static BindableProperty IsSelectedProperty =
                    BindableProperty.Create(nameof(IsSelected),
                                            typeof(bool),
                                            typeof(TabButton),
                                            propertyChanged: (b, o, n) => (b as TabButton).IsSelectedChanged());
        private void IsSelectedChanged()
        {
            BackgroundColor = IsSelected ? Color.FromHex("#305252") : Color.FromHex("#488286");

        }
        public bool IsSelected
        {
            get => (bool)GetValue(IsSelectedProperty);
            set => SetValue(IsSelectedProperty, value);
        }
        #endregion
        #region Text
        public static BindableProperty TextProperty =
                    BindableProperty.Create(nameof(Text),
                                            typeof(string),
                                            typeof(TabButton),
                                            propertyChanged: (b, o, n) => (b as TabButton).TextChanged());
        private void TextChanged()
        {
            label.Text = Text;
        }
        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }
        #endregion

        public event EventHandler<TabEventArgs> Selected;

        public TabButton()
        {
            InitializeComponent();
            IsSelectedChanged();
        }

        private void Tapped(object sender, EventArgs e)
        {
            if(!IsSelected)
            {
                Selected?.Invoke(this, new TabEventArgs
                {
                    ViewModel = BindingContext as ImageGroupViewModel
                });
            }
        }
    }

    public class TabEventArgs : EventArgs
    {
        public ImageGroupViewModel ViewModel { get; set; }
    }
}
