﻿using System.Runtime.CompilerServices;
using ImageGallery.GridLayout.Abstractions;
using Xamarin.Forms;

namespace ImageGallery.Views
{
    public partial class ImageGrid : Grid
    {
        #region Layouter
        public static BindableProperty LayouterFactoryProperty =
                    BindableProperty.Create(nameof(LayouterFactory),
                                            typeof(IGridLayouterFactory),
                                            typeof(ImageGrid));
        public IGridLayouterFactory LayouterFactory
        {
            get => (IGridLayouterFactory)GetValue(LayouterFactoryProperty);
            set => SetValue(LayouterFactoryProperty, value);
        }
        #endregion

        private IGridLayouter layouter;
        public ImageGrid()
        {
            InitializeComponent();
        }
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == BindableLayout.ItemsSourceProperty.PropertyName)
            {
                layouter?.Dispose();
                layouter = LayouterFactory.GetLayouter(this);
            }
        }
        protected override void OnChildAdded(Element child)
        {
            base.OnChildAdded(child);
            layouter.LayoutChild(child as View);
        }
    }
}
