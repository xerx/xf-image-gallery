﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace ImageGallery.Views
{
    public partial class ImageView : ContentView
    {
        #region Source
        public static BindableProperty SourceProperty =
                    BindableProperty.Create(nameof(Source),
                                            typeof(ImageSource),
                                            typeof(ImageView),
                                            propertyChanged: (b, o, n) => (b as ImageView).SourceChanged());
        private void SourceChanged()
        {
            image.Source = Source;
        }
        public ImageSource Source
        {
            get => (ImageSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }
        #endregion
        #region Command
        public static BindableProperty CommandProperty =
                    BindableProperty.Create(nameof(Command),
                                            typeof(ICommand),
                                            typeof(ImageView));
        public ICommand Command
        {
            get => (ICommand)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }
        #endregion
        public ImageView()
        {
            InitializeComponent();
        }
        private void Selected(object sender, EventArgs e)
        {
            if(Command?.CanExecute(BindingContext) ?? false)
            {
                Command.Execute(BindingContext);
            }
        }
    }
}
