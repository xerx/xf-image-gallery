﻿using System.Collections.Generic;

namespace ImageGallery.Models
{
    public class ImageGroup
    {
        public string Id { get; set; }
        public string Icon { get; set; }
        public IEnumerable<Image> Images { get; set; }
    }
}
