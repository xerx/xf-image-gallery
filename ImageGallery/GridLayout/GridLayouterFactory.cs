﻿using ImageGallery.Extensions;
using ImageGallery.GridLayout.Abstractions;
using ImageGallery.GridLayout.Layouters;
using ImageGallery.Views;
using Xamarin.Forms;

namespace ImageGallery.Services
{
    public class GridLayouterFactory : IGridLayouterFactory
    {
        public IGridLayouter GetLayouter(ImageGrid grid)
        {
            int numChildren = BindableLayout.GetItemsSource(grid).Count();
            switch (numChildren)
            {
                case 0:
                case 1: return new GridLayouter1(grid);
                case 2: return new GridLayouter2(grid);
                case 3: return new GridLayouter3(grid);
                case 4: return new GridLayouter4(grid);
                case 5: return new GridLayouter5(grid);
                case 6: return new GridLayouter6(grid);
                case 7: return new GridLayouter7(grid);
                case 8: return new GridLayouter8(grid);
                default: return new GridLayouter9(grid);
            }
        }
    }
}
