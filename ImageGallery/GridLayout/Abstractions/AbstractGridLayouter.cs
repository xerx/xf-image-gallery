﻿using System;
using ImageGallery.Extensions;
using ImageGallery.Views;
using Xamarin.Forms;

namespace ImageGallery.GridLayout.Abstractions
{
    public abstract class AbstractGridLayouter : IGridLayouter
    {
        protected ImageGrid grid;
        public AbstractGridLayouter(ImageGrid grid)
        {
            this.grid = grid;
        }
        public abstract void LayoutChild(View child);
        protected int GetChildIndex(View child)
        {
            try
            {
                return grid.Children.IndexOf(child);
            }
            catch (Exception ex)
            {
                ex.Dump();
                return 0;
            }
        }
        public void Dispose()
        {
            grid = null;
        }
    }
}
