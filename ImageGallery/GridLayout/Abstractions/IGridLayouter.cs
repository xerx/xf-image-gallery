﻿using System;
using Xamarin.Forms;

namespace ImageGallery.GridLayout.Abstractions
{
    public interface IGridLayouter : IDisposable
    {
        void LayoutChild(View child);
        
    }
}
