﻿using ImageGallery.Views;

namespace ImageGallery.GridLayout.Abstractions
{
    public interface IGridLayouterFactory
    {
        IGridLayouter GetLayouter(ImageGrid grid);
    }
}
