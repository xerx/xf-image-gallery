﻿using ImageGallery.GridLayout.Abstractions;
using ImageGallery.Views;
using Xamarin.Forms;

namespace ImageGallery.GridLayout.Layouters
{
    public class GridLayouter3 : AbstractGridLayouter
    {
        public GridLayouter3(ImageGrid grid) : base(grid) { }
        public override void LayoutChild(View child)
        {
            int index = GetChildIndex(child);
            switch (index)
            {
                case 0:
                    Grid.SetRowSpan(child, 3);
                    Grid.SetColumnSpan(child, 2);
                    break;
                case 1:
                    Grid.SetRowSpan(child, 2);
                    Grid.SetColumn(child, 2);
                    break;
                case 2:
                    Grid.SetRow(child, 2);
                    Grid.SetColumn(child, 2);
                    break;
            }
        }
    }
}
