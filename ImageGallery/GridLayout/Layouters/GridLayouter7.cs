﻿using ImageGallery.GridLayout.Abstractions;
using ImageGallery.Views;
using Xamarin.Forms;

namespace ImageGallery.GridLayout.Layouters
{
    public class GridLayouter7 : AbstractGridLayouter
    {
        public GridLayouter7(ImageGrid grid) : base(grid) { }
        public override void LayoutChild(View child)
        {
            int index = GetChildIndex(child);
            switch (index)
            {
                case 1:
                    Grid.SetColumn(child, 1);
                    break;
                case 2:
                    Grid.SetColumn(child, 2);
                    Grid.SetRowSpan(child, 2);
                    break;
                case 3:
                    Grid.SetRow(child, 1);
                    break;
                case 4:
                    Grid.SetRow(child, 1);
                    Grid.SetColumn(child, 1);
                    break;
                case 5:
                    Grid.SetRow(child, 2);
                    break;
                case 6:
                    Grid.SetRow(child, 2);
                    Grid.SetColumn(child, 1);
                    Grid.SetColumnSpan(child, 2);
                    break;
            }
        }
    }
}
