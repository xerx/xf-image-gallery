﻿using ImageGallery.GridLayout.Abstractions;
using ImageGallery.Views;
using Xamarin.Forms;

namespace ImageGallery.GridLayout.Layouters
{
    public class GridLayouter1 : AbstractGridLayouter
    {
        public GridLayouter1(ImageGrid grid) : base(grid) { }
        override public void LayoutChild(View child)
        {
            Grid.SetRowSpan(child, 3);
            Grid.SetColumnSpan(child, 3);
        }
    }
}
