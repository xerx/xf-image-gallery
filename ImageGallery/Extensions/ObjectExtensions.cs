﻿using System.Diagnostics;

namespace ImageGallery.Extensions
{
    public static class ObjectExtensions
    {
        public static void Dump(this object @object)
        {
            var message = @object != null ? $"> {@object.GetType().Name}: {@object}"
                                          : "> Cannot dump null object";
            Debug.WriteLine(message);
        }
    }
}
