﻿using System.Threading.Tasks;
using ImageGallery.Views;
using Prism.Navigation;

namespace ImageGallery.Extensions
{
    public static class NavigationExtensions
    {
        public static Task<INavigationResult> NavigateToHome(this INavigationService navigationService) =>
            navigationService.NavigateAsync(nameof(HomePage));
    }
}
