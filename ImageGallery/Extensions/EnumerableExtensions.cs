﻿using System;
using System.Collections;

namespace ImageGallery.Extensions
{
    public static class EnumerableExtensions
    {
        public static int Count(this IEnumerable enumerable)
        {
            int sum = 0;
            foreach (var item in enumerable)
            {
                sum++;
            }
            return sum;
        }
    }
}
