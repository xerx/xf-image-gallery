﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace ImageGallery.Converters
{
    public class PathToResourceImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var path = value?.ToString();
            if(!string.IsNullOrEmpty(path))
            {
                return ImageSource.FromResource(path);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
