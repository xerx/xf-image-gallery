﻿using System.Collections.Generic;
using ImageGallery.Models;

namespace ImageGallery.Services.Abstractions
{
    public interface IDataProvider
    {
        IEnumerable<ImageGroup> GetImageGroups();
    }
}
