﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using ImageGallery.Helpers;
using ImageGallery.Models;
using ImageGallery.Services.Abstractions;

namespace ImageGallery.Services
{
    public class DataProvider : IDataProvider
    {
        public IEnumerable<ImageGroup> GetImageGroups()
        {
            var imagePaths = LoadImagePaths();
            return imagePaths.GroupBy(path => GetGroupId(path))
                             .Select(group => CreateImageGroup(group))
                             .ToList();
        }

        private static ImageGroup CreateImageGroup(IGrouping<string, string> group)
        {
            return new ImageGroup
            {
                Id = group.Key,
                Icon = GetGroupIcon(group.Key),
                Images = group.Select(path => new Image { Path = path })
            };
        }

        private static string GetGroupIcon(string id)
        {
            switch(id)
            {
                case "boat": return IconFont.SailBoat;
                case "car": return IconFont.CarHatchback;
                case "heli": return IconFont.Helicopter;
                default: return IconFont.Close;
            }
        }

        private static string GetGroupId(string imagePath)
        {
            return Regex.Match(imagePath, @"([a-z]|[A-Z])+(?=\d+.jpg)").ToString();
        }

        private static IEnumerable<string> LoadImagePaths()
        {
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(DataProvider)).Assembly;
            return assembly.GetManifestResourceNames().Where(name => name.EndsWith(".jpg"));
        }

    }
}

